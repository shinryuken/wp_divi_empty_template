<?php
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 
?>
<style>
#content-area {max-width: 900px;margin:0px auto;}
#sidebar {
    float: left;
    width: 20.875%;
	padding-right: 30px;
	padding-left: 0px;
	margin-top: 8vw;
}
#left-area {
    float: right;
    padding-left: 5.5%;
}
.et_main_title h1 {
     display: block;

}
.et_pb_row{width:100%}
.p_title {
    margin-top: 3vw;
    margin-bottom: 3vw;
}
@media (max-width: 981px)
{
	.news_sidebar,#sidebar{display:none;}
	.et_pb_row{width:80%}
}
</style>
<div id="main-content">

	<div id="top_banner">
		<div class="et_pb_module et-waypoint et_pb_fullwidth_image et_pb_animation_off slider_top et_pb_fullwidth_image_0 et-animated">
			<img src="<?php echo get_stylesheet_directory_uri();?>/images/banner_mask.png" alt="">
		</div>
		<?php 
		$cat_name="最新消息";
		/*
		$src="";
		$categories = get_the_category();
		//print_r(get_categories());
		$cat_name=$categories[0]->name;
		if ( ! empty( $categories ) ) {
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $categories[0]->description, $matches);
			$src = $matches[1][0];
		}
		if($src!="")
		{
			echo '<img src="'.$src.'" width="100%" alt="'.$cat_name.'" title="'.$cat_name.'">';
		}
		else
		{
			echo '<img src="http://www.cctt.org.tw/wp-content/uploads/2019/02/news_banner.jpg" width="100%">';
		}
		*/
		echo '<img src="http://www.cctt.org.tw/wp-content/uploads/2019/02/news_banner.jpg" width="100%">';
		?>
	
	</div>
	<div class="container" >
	<?php
		//加入導航功能
		if ( function_exists('yoast_breadcrumb') ) {
		  yoast_breadcrumb( '<div id="breadcrumb"><p id="breadcrumbs">','</p></div>' );
		}
	?>
		<div id="content-area" class="clearfix">
	
		<div id="sidebar">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("news_sidebar") ) : ?>
			<?php endif;?>	
		</div>
		<div id="left-area">
		<div class="et_pb_section  et_pb_section_0 et_section_regular">
			<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center p_title et_pb_text_0">		
				<h6 class="has-medium-font-size"><?php echo $cat_name;?></h6>
			</div>
		</div>
		
		<div class=" et_pb_row et_pb_row_1">
			<div class=news_list>
			<?php
			
			$args = array(
				'tax_query' => array(
					'relation' => 'or',
					array(
						'taxonomy' => 'category',
						'field'    => 'slug',
						'terms'    => array('news'),
					),
					array(
						'taxonomy' => 'wpdmcategory',
						'field'    => 'slug',
						'terms'    => array( 'donate' ),
					),
				),
			);
			query_posts($args );
		
			?>
			<?php while ( have_posts() ) : the_post();  ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>
				
				<!--
				<div class=news_img>
				<?php
					$post_thumbnail_id = get_post_thumbnail_id();
					$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
					
				?>
					<?php if($post_thumbnail_url!=""){?>
					<a href="<?php the_permalink(); ?>" rel="bookmark" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><img title="<?php the_title(); ?>" alt="<?php the_title(); ?>" class="" src="<?php echo $post_thumbnail_url; ?>" width=100%></a>
					<?php }else{ echo "<div class='news_noimg'>".$cat_name."</div>";}?>
				</div>
				-->
				
				<?php if(!has_term('donate','wpdmcategory',get_the_ID())){?>
				<div class=news_intro>
					<a href="<?php the_permalink(); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="news_intro_title"><span class="news_intro_title_date"><?php echo get_the_date()."  ";?></span> [消息]<?php the_title(); ?></a>
					<!--
					<div class="news_intro_content">	
						<?php the_excerpt();?>	
						<br><a href="<?php the_permalink(); ?>" rel="bookmark" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="news_intro_more">...繼續閱讀</a>
					</div>
					-->
				</div>
				<?php }else{  ?>
				<div class=news_intro>
					<a href="/?wpdmdl=<?php wp_get_attachment_url(the_ID()) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="news_intro_title">
					<span class="news_intro_title_date"><?php echo get_the_date()."  ";?></span> [徵信]<?php the_title(); ?>
				   </a>
				</div>
				<?php }  ?>
				
				
				<?php endif;  ?>



				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

				</div> <!-- #news_list -->
			
			</div> <!--	et_pb_row -->
			</div> <!--	et_pb_section -->

			</div> <!-- #left-area -->

			<?php //get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->



</div> <!-- #main-content -->

<?php get_footer(); ?>