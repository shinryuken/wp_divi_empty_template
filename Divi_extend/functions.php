<?php
/**
 divi project 刪除
 */
add_filter( 'et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1 );
function mytheme_et_project_posttype_args( $args ) {
	return array_merge( $args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}

/**
新增header 文字

add_action( 'et_header_top', 'custom_header_top', 10, 1 );
function custom_header_top( ) {
	$html="<div id='header_contact'>台南:06-4236777<br>
			高雄:07-7676829</div>";
	echo $html;
	//printf(__($html, 'divi'));	
}
 */
/*

/*
為編輯權限,移除特定選單
*/
function remove_menus(){
// get current login user's role
$roles = wp_get_current_user()->roles;
 
if( !in_array('editor',$roles)){
return;
}
//echo '<pre>' . print_r( $GLOBALS[ 'menu' ], TRUE) . '</pre>';
remove_menu_page( 'index.php' ); 
remove_menu_page( 'edit-comments.php' ); 
remove_menu_page( 'tools.php' );


}
add_action( 'admin_menu', 'remove_menus' , 100 );



/*
幫全部post type加上Divi Builder

add_filter('et_builder_post_types', 'divicolt_post_types');
add_filter('et_fb_post_types','divicolt_post_types' ); // Enable Divi Visual Builder on the custom post types
function divicolt_post_types($post_types)
{
    foreach (get_post_types() as $post_type) {
        if (!in_array($post_type, $post_types) and post_type_supports($post_type, 'editor')) {
            $post_types[] = $post_type;
        }
    }
    return $post_types;
}
*/
/*
幫全部post type加上DIvi page settings

add_action('add_meta_boxes', 'divicolt_add_meta_box');
function divicolt_add_meta_box()
{
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'editor') and function_exists('et_single_settings_meta_box')) {
            $obj= get_post_type_object( $post_type );
            add_meta_box('et_settings_meta_box', sprintf(__('Divi %s Settings', 'Divi'), $obj->labels->singular_name), 'et_single_settings_meta_box', $post_type, 'side', 'high');
        }
    }
}
*/
/*





/*
註冊一個最新消息sidebar


if ( function_exists('register_sidebar') )
  register_sidebar(array(
	'name'          => __( '最新消息左側顯示欄位', 'DIVI' ),
	'id'            => 'news_sidebar',
    'before_widget' => '<div class = "news_sidebar">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);
*/


/*
註冊一個圖片與連結的widget

function wpb_load_widget() {
    register_widget( 'img_show_link' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
 
 
class img_show_link extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		 
		// Base ID of your widget
		'img_show_link', 
		 
		// Widget name will appear in UI
		__('側邊固定圖片模組', 'img_show_link_domain'), 
		 
		// Widget description
		array( 'description' => __( '側邊固定圖片-設定圖片網址與連結就可以', 'img_show_link_domain' ), ) 
		);
	}
	 
	// Creating widget front-end
	 
	public function widget( $args, $instance ) {
		$img_url = apply_filters( 'widget_img_url', $instance['img_url'] );
		$img_link = apply_filters( 'widget_img_link', $instance['img_link'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $img_url ) )
		echo $args['before_title'] ;
		echo "<a href='".$img_link."'><img src='".$img_url ."' width=100%></a>". $args['after_title'];
		echo $args['after_widget'];
	}
			 
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'img_url' ] ) ) {
			$img_url = $instance[ 'img_url' ];
			$img_link = $instance[ 'img_link' ];
		}
		else {
			$img_url = __( '', 'img_show_link_domain' );
			$img_link = __( '', 'img_show_link_domain' );
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'img_url' ); ?>"><?php _e( '圖片網址:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'img_url' ); ?>" name="<?php echo $this->get_field_name( 'img_url' ); ?>" type="text" value="<?php echo esc_attr( $img_url ); ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'img_link' ); ?>"><?php _e( '圖片連結:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'img_link' ); ?>" name="<?php echo $this->get_field_name( 'img_link' ); ?>" type="text" value="<?php echo esc_attr( $img_link ); ?>" />
		</p>
		<?php 
	}
		 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['img_url'] = ( ! empty( $new_instance['img_url'] ) ) ? strip_tags( $new_instance['img_url'] ) : '';
		$instance['img_link'] = ( ! empty( $new_instance['img_link'] ) ) ? strip_tags( $new_instance['img_link'] ) : '';
		return $instance;
	}
}// Class img_show_link ends here
*/