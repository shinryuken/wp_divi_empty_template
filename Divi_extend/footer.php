<?php
/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
do_action( 'et_after_main_content' );

if ( 'on' === et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

			
				
				<div id="footer-bottom">
					<div class="container clearfix">
					<div id="footer_img"><img src='/wp-content/uploads/2019/05/logo2.png'></div>
					<div id="footer_nav" class="wp-block-columns has-2-columns">
							<div class="wp-block-column">
							
								<div class="footer_title footer_title">關於我們</div>
								台南總公司:<br>
								台南市永康區國聖街301號<br>
								TEL 06-2436777  FAX 06-2436988<br>
								<br>
								高雄分公司:<br>
								高雄市鳳山區建國路三段96號<br>
								TEL 07-7676829  FAX 07-7678238

							</div>
							

							<div class="wp-block-column">
								<div class="footer_title footer_title">活動花絮</div>
								<?php
								/*
								$args = array(
									'taxonomy'           => 'aigpl_cat',
									'hide_empty'         => 0,
									'orderby'            => 'name',
									'order'              => 'ASC',
									'show_count'         => 0,
									'use_desc_for_title' => 0,
									'title_li'           => 0,
									'child_of'=>18,
								);
								echo '<ul>';
								$categories = wp_list_categories($args);
								echo '</ul>';
								*/
								
								?>
								<ul>
								<li><a href="/photos">所有花絮</a></li>
								<li><a href="/community">社區融合</a></li>
								<li><a href="/activity">活動參與</a></li>
								<li><a href="/care">送暖關懷</a></li>	
								<li><a href="/volunteer">志工服務</a></li>
								
								</ul>
							</div>
							
					</div>		
					</div>	<!-- .container -->
					<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
					echo et_core_fix_unclosed_html_tags( et_core_esc_previously( et_get_footer_credits() ) );
					// phpcs:enable
					?>
					<!--
					<div id=copyright>
					Copyright © 2019 財團法人台南市私立長泰教養院新市分院 <br>
					Private Changtai and Nursing Institute of Tainan, Sinshih<br>
					Designed by 優勢方舟數位行銷股份有限公司
					</div>
					-->
				</div>
				
				
				
				
				
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>
