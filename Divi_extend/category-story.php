<?php
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/js/fancybox/jquery.fancybox.min.css" />
<script src="<?php echo get_stylesheet_directory_uri();?>/js/fancybox/jquery.fancybox.min.js"></script>
<style>
#left-area {
    float: none;
    width: 100% !important;
    padding-right: 0%;
}
#product_list{margin-bottom:0px;}
.product_item{max-height:320px;height:100%;width: 31%;margin:0% 1%;float:left;display:block;overflow:hidden;}
.product_img{position: relative;overflow:hidden;width:100%}
.product_img img{transition: .5s ease;backface-visibility: hidden;}
.product_intro{text-align:Center;}
.et_pb_section_0.et_pb_section {
    padding-bottom: 0px;
}
.et_pb_section_1.et_pb_section {
	padding-top: 0px;
    padding-bottom: 0px;
}
.more_btn{   

	font-size:16px;

	}
@media (max-width: 981px)
{
	.news_sidebar,#sidebar{display:none;}
	.et_pb_row{width:80%}
}
</style>
<div id="main-content">
	
	<div id="top_banner">
		<div class="et_pb_module et-waypoint et_pb_fullwidth_image et_pb_animation_off slider_top et_pb_fullwidth_image_0 et-animated">
			<img src="<?php echo get_stylesheet_directory_uri();?>/images/banner_mask.png" alt="">
		</div>
		<?php 
		$cat_name="心情故事";
		
		echo '<img src="http://www.cctt.org.tw/wp-content/uploads/2019/02/photos_banner.jpg" width="100%">';
		?>
	
	</div>

	<div class="container" >
	<?php
		//加入導航功能
		if ( function_exists('yoast_breadcrumb') ) {
		  yoast_breadcrumb( '<div id="breadcrumb"><p id="breadcrumbs">','</p></div>' );
		}
	?>
		<div id="content-area" class="clearfix">
	
		<div id="left-area">
		
		<div class="et_pb_section  et_pb_section_0 et_section_regular">		
			<div class="et_pb_row et_pb_row_0 et_pb_row_4col">
				<div class="et_pb_column et_pb_column_1_4  et_pb_column_0">
				
				<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/photos" data-icon="5">所有花絮</a>
			</div><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_1 et_pb_module et_pb_bg_layout_light" href="/volunteer" data-icon="5">志工服務</a>
			</div>
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_1">
				
				<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_2 et_pb_module et_pb_bg_layout_light" href="/community" data-icon="5">社區適應</a>
			</div><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_3 et_pb_module et_pb_bg_layout_light" href="/training" data-icon="5">教育訓練</a>
			</div>
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_2">
				
				<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_4 et_pb_module et_pb_bg_layout_light" href="/activity" data-icon="5">活動參與</a>
			</div><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_5 et_pb_module et_pb_bg_layout_light" href="/honor" data-icon="5">　榮譽榜</a>
			</div>
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_3">
				
				<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn et_pb_button_6 et_pb_module et_pb_bg_layout_light" href="/care" data-icon="5">送暖關懷</a>
			</div><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button et_pb_custom_button_icon more_btn btn_active et_pb_button_7 et_pb_module et_pb_bg_layout_light" href="/category/story" data-icon="5">心情故事</a>
			</div>
			</div> <!-- .et_pb_column -->
			</div> <!-- .et_pb_row -->
				
		</div>
		
		<div class="et_pb_section  et_pb_section_1 et_section_regular">
			<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center p_title et_pb_text_0">		
				<h6 class="has-medium-font-size"><?php echo $cat_name;?></h6>
			</div>
		</div>
		
		
		<div class=" et_pb_row et_pb_row_1">	
				
			<div class=news_list>   
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if ( ! $is_page_builder_used ) : ?>
				
			
				<div class=news_intro>
					<a href="<?php the_permalink(); ?>" rel="bookmark" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="news_intro_title"><span class="news_intro_title_date"><?php echo get_the_date()."  ";?></span> <?php the_title(); ?></a>
					<!--
					<div class="news_intro_content">	
						<?php the_excerpt();?>	
						<br><a href="<?php the_permalink(); ?>" rel="bookmark" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="news_intro_more">...繼續閱讀</a>
					</div>
					-->
				</div>
				
				
				
				<?php endif;  ?>


			<?php endwhile; ?>
			<div class=clear></div>	
			</div><!--end product_list-->	
			
			</div> <!--	et_pb_row -->
			</div> <!--	et_pb_section -->
			</div> <!-- #left-area -->

			<?php// get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->



</div> <!-- #main-content -->

<?php get_footer(); ?>